// Copyright © 2023-2024 FaNtic, All rights reserved

#include "Framework/TSGameModeBase.h"
#include "Framework/TSPawnBase.h"
#include "Game/TSGridBase.h"

void ATSGameModeBase::BeginPlay()
{
	check(GetWorld());

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	check(PlayerController);

	ATSPawnBase* Pawn = Cast<ATSPawnBase>(PlayerController->GetPawn());
	check(Pawn);
}