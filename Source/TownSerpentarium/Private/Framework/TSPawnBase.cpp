// Copyright © 2023-2024 FaNtic, All rights reserved

#include "Framework/TSPawnBase.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "Game/TSGridBase.h"
#include "Game/TSSnakeActorBase.h"
#include "Game/TSFoodRandomSpawnerBase.h"

namespace
{
	float GetRadHalfHFOV(UCameraComponent* Camera)
	{
		return FMath::DegreesToRadians(Camera->FieldOfView * 0.5);
	}
	float GetRadHalfVFOV(UCameraComponent* Camera, float VieportAspectRatio)
	{
		return FMath::Atan(FMath::Tan(FMath::DegreesToRadians(0.5f * Camera->FieldOfView)) * 1 / VieportAspectRatio);
	}
	bool IsEven(int32 Number)
	{
		return !static_cast<bool>(Number % 2);
	}
}

ATSPawnBase::ATSPawnBase()
{
	PrimaryActorTick.bCanEverTick = true;

	// Creating CameraComponent and setting it to RootComponent with directly TopDownView
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetWorldRotation(FRotator(-90.0f, 0.0f, 0.0f));
	check(CameraComponent);
	RootComponent = CameraComponent;
}

void ATSPawnBase::BeginPlay()
{
	Super::BeginPlay();

	SpawnGrid();
	UpdatePawnLocationByGrid();
	
	SpawnSnake();

	SpawnFoodSpawner();
}

void ATSPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATSPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveVertical", this, &ATSPawnBase::OnMoveVertical);
	PlayerInputComponent->BindAxis("MoveHorizontal", this, &ATSPawnBase::OnMoveHorizontal);
}

void ATSPawnBase::OnMoveVertical(float InputAxisValue)
{
	if (!IsValid(Snake))
		return;

	if (!Snake->SnakeMoved)
		return;

	if (InputAxisValue == 1.0f && Snake->LastMovementDirection != EMovementDirection::Down)
	{
		Snake->LastMovementDirection = EMovementDirection::Up;
		Snake->SnakeMoved = false;
	}
	else if (InputAxisValue == -1.0f && Snake->LastMovementDirection != EMovementDirection::Up)
	{
		Snake->LastMovementDirection = EMovementDirection::Down;
		Snake->SnakeMoved = false;
	}
}

void ATSPawnBase::OnMoveHorizontal(float InputAxisValue)
{
	if (!IsValid(Snake))
		return;

	if (!Snake->SnakeMoved)
		return;

	if (InputAxisValue == 1.0f && Snake->LastMovementDirection != EMovementDirection::Left)
	{
		Snake->LastMovementDirection = EMovementDirection::Right;
		Snake->SnakeMoved = false;
	}
	else if (InputAxisValue == -1.0f && Snake->LastMovementDirection != EMovementDirection::Right)
	{
		Snake->LastMovementDirection = EMovementDirection::Left;
		Snake->SnakeMoved = false;
	}
}

void ATSPawnBase::SpawnGrid()
{
	check(GetWorld());

	Grid = GetWorld()->SpawnActor<ATSGridBase>(GridClass, GridTransform);
	check(Grid);
}

void ATSPawnBase::UpdatePawnLocationByGrid()
{
	check(GEngine);
	check(GEngine->GameViewport);
	check(GEngine->GameViewport->Viewport);

	auto* Viewport = GEngine->GameViewport->Viewport;
	Viewport->ViewportResizedEvent.Remove(ViewportResizedHandle);
	ViewportResizedHandle = Viewport->ViewportResizedEvent.AddUObject(this, &ATSPawnBase::OnViewportResized);

#if WITH_EDITOR
	OnViewportResized(Viewport, 0);
#endif
}

void ATSPawnBase::OnViewportResized(FViewport* Viewport, uint32 Value)
{
	check(Grid)

	if (!Viewport || Viewport->GetSizeXY().Y == 0 || Grid->Dimensions.X == 0)
		return;

	const float WorldWidth = Grid->Dimensions.Y * Grid->CellSize;
	const float WorldHeight = Grid->Dimensions.X * Grid->CellSize;

	const float ViewportAspectRatio = static_cast<float>(Viewport->GetSizeXY().X) / Viewport->GetSizeXY().Y;
	const float GridAspectRatio = static_cast<float>(Grid->Dimensions.Y) / Grid->Dimensions.Y;

	check(ViewportAspectRatio);

	float LocationZ = 0.0f;

	if (ViewportAspectRatio <= GridAspectRatio)
		LocationZ = 0.5f * WorldWidth / FMath::Tan(GetRadHalfHFOV(CameraComponent));
	else
		LocationZ = 0.5f * WorldHeight / FMath::Tan(GetRadHalfVFOV(CameraComponent, ViewportAspectRatio));

	LocationZ = LocationZ * (1 + CameraOffsetByZ);

	const FVector NewPawnLocation = GridTransform.GetLocation() + FVector(0.0f, 0.0f, LocationZ);
	SetActorLocation(NewPawnLocation);
}

void ATSPawnBase::SpawnSnake()
{
	check(GetWorld());

	FVector SnakeSpawnLocation = GridTransform.GetLocation() + SnakeSpawnOffset;
	FTransform SnakeSpawnTransform = FTransform(SnakeSpawnLocation);
	
	Snake = GetWorld()->SpawnActorDeferred<ATSSnakeActorBase>(SnakeClass, SnakeSpawnTransform);

	Snake->Pawn = this;
	Snake->ElementOffset = Grid->CellSize;
	Snake->ElementScale = GridTransform.GetScale3D() * SnakeElementScaleMultiplier;

	Snake->FinishSpawning(SnakeSpawnTransform);
}

void ATSPawnBase::SpawnFoodSpawner()
{
	check(GetWorld());
	
	FoodSpawner = GetWorld()->SpawnActorDeferred<ATSFoodRandomSpawnerBase>(FoodSpawnerClass, FTransform::Identity);
	FoodSpawner->Snake = Snake;
	FoodSpawner->FinishSpawning(FTransform::Identity);

	check(FoodSpawner);
}
