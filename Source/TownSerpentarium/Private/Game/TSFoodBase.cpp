// Copyright © 2023-2024 FaNtic, All rights reserved

#include "Game/TSFoodBase.h"
#include "Game/TSSnakeActorBase.h"
#include "Game/TSFoodRandomSpawnerBase.h"

ATSFoodBase::ATSFoodBase()
{
	PrimaryActorTick.bCanEverTick = false;

	FoodStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Food Mesh"));
	check(FoodStaticMeshComponent);
	FoodStaticMeshComponent->SetupAttachment(RootComponent);
}

void ATSFoodBase::Interact(AActor* Interactor)
{
	ATSSnakeActorBase* SnakeActor = Cast<ATSSnakeActorBase>(Interactor);
	if (!IsValid(SnakeActor))
		return;

	this->Destroy();

	SnakeActor->AddSnakeElement(ElementsToAddAmount);

	SnakeActor->UpdateAvaliableLocations();
	FoodSpawner->SpawnNewFood(SnakeActor->AvaliableCoordinates);
}
