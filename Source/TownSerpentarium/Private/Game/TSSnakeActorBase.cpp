// Copyright © 2023-2024 FaNtic, All rights reserved

#include "Framework/TSPawnBase.h"
#include "Game/TSSnakeActorBase.h"
#include "Game/TSSnakeElementBase.h"
#include "Game/TSGridBase.h"
#include "Interfaces/TSInteractable.h"
#include "UObject/UObjectGlobals.h"
#include "Kismet/GameplayStatics.h"

ATSSnakeActorBase::ATSSnakeActorBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATSSnakeActorBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(UpdateMoveTime);

	InitSnake(4);
	UpdateAvaliableLocations();

	FTimerHandle StartTimeHandle;
	GetWorldTimerManager().SetTimer(StartTimeHandle, this, &ATSSnakeActorBase::StartTickAfterDelay, StartGameDelay, false);
}

void ATSSnakeActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!GameStarted)
	{
		TimeToStartLeft -= DeltaTime;
		return;
	}

	Move();
	SnakeMoved = true;
}

void ATSSnakeActorBase::InitSnake(uint32 SnakeLength)
{
	check(GetWorld());

	// Adding snake head element
	FTransform NewSnakeElementTransform = FTransform(GetActorTransform().GetRotation(), GetActorTransform().GetLocation(), ElementScale);

	SnakeHead = GetWorld()->SpawnActor<ATSSnakeElementBase>(SnakeHeadClass, NewSnakeElementTransform);
	SnakeElements.AddHead(SnakeHead);

	SnakeHead->SnakeOwner = this;

	// Adding snake body element [---<]
	for (uint32 i = 1; i < SnakeLength; ++i)
	{
		FVector NewSnakeElementLocation = SnakeHead->GetActorLocation() - FVector(0.0f, ElementOffset * i, 0.0f);
		NewSnakeElementTransform = FTransform(GetActorTransform().GetRotation(), NewSnakeElementLocation, ElementScale);

		auto NewSnakeBodyElement = GetWorld()->SpawnActor<ATSSnakeElementBase>(SnakeBodyClass, NewSnakeElementTransform);
		SnakeElements.AddTail(NewSnakeBodyElement);

		NewSnakeBodyElement->SnakeOwner = this;
	}
}

void ATSSnakeActorBase::AddSnakeElement(uint32 ElementsNum)
{
	check(GetWorld());

	for (uint32 i = 0; i < ElementsNum; ++i)
	{
		FTransform NewSnakeElementTransform = SnakeElements.GetTail()->GetValue()->GetActorTransform();
		auto NewSnakeBodyElement = GetWorld()->SpawnActor<ATSSnakeElementBase>(SnakeBodyClass, NewSnakeElementTransform);
		NewSnakeBodyElement->SetActorScale3D(ElementScale);
		SnakeElements.AddTail(NewSnakeBodyElement);

		NewSnakeBodyElement->SnakeOwner = this;
	}
}

void ATSSnakeActorBase::KillSnake()
{	
	this->Destroy();

	double DestroyElementDelayTime = 0.1f;
	for (ATSSnakeElementBase* SnakeElement : SnakeElements)
	{
		SnakeElement->DestroyElementAfterTime(DestroyElementDelayTime);
		DestroyElementDelayTime += 0.1f;
	}
}

void ATSSnakeActorBase::DestroyElement()
{	
	ATSSnakeElementBase* FirstElement = SnakeElements.GetHead()->GetValue();
	
	if (IsValid(FirstElement))
		FirstElement->Destroy();
}

void ATSSnakeActorBase::StartTickAfterDelay()
{
	GameStarted = true;
}

void ATSSnakeActorBase::Move()
{
	FVector MovementVector = FVector::ZeroVector;
	float DeltaMoveDistance = ElementOffset;

	switch (LastMovementDirection)
	{
	case EMovementDirection::Up:
		MovementVector.X += DeltaMoveDistance;
		break;
	case EMovementDirection::Down:
		MovementVector.X -= DeltaMoveDistance;
		break;
	case EMovementDirection::Left:
		MovementVector.Y -= DeltaMoveDistance;
		break;
	case EMovementDirection::Right:
		MovementVector.Y += DeltaMoveDistance;
		break;
	}

	FVector HeadPreviousLocation = SnakeHead->GetActorLocation();

	ATSSnakeElementBase* SnakeTail = SnakeElements.GetTail()->GetValue();
	SnakeTail->SetActorEnableCollision(false);
	SnakeTail->SetActorLocation(HeadPreviousLocation);
	SnakeElements.InsertNode(SnakeTail, SnakeElements.GetHead()->GetNextNode());
	SnakeElements.RemoveNode(SnakeElements.GetTail());

	SnakeHead->AddActorWorldOffset(MovementVector);
	SnakeTail->SetActorEnableCollision(true);
}

void ATSSnakeActorBase::UpdateAvaliableLocations()
{
	check(Pawn);
	check(Pawn->Grid);

	TArray<FIntPoint> SnakeElementCoordinates;
	for (ATSSnakeElementBase* Element : SnakeElements)
		SnakeElementCoordinates.Add(FIntPoint(Element->GetActorLocation().X, Element->GetActorLocation().Y));

	FIntPoint EmptyGridDimensions = FIntPoint(Pawn->Grid->Dimensions.X - 2, Pawn->Grid->Dimensions.Y - 2);

	const float WorldWidth = EmptyGridDimensions.Y * Pawn->Grid->CellSize;
	const float WorldHeight = EmptyGridDimensions.X * Pawn->Grid->CellSize;

	const FVector StartPosition = Pawn->Grid->GetActorLocation() - FVector(-WorldHeight / 2 + Pawn->SnakeSpawnOffset.X, WorldWidth / 2 - Pawn->SnakeSpawnOffset.Y, 0.0f);

	TArray<FIntPoint> Locations;
	for (int32 i = 0; i < EmptyGridDimensions.X; ++i)
	{
		for (int32 j = 0; j < EmptyGridDimensions.Y; ++j)
		{
			FIntPoint AvaliableCoordinate = FIntPoint(StartPosition.X - Pawn->Grid->CellSize * i, StartPosition.Y + Pawn->Grid->CellSize * j);

			if (!SnakeElementCoordinates.Contains(AvaliableCoordinate))
				Locations.Add(AvaliableCoordinate);
		}
	}

	AvaliableCoordinates = Locations;
}

void ATSSnakeActorBase::SnakeElementOverlapped(ATSSnakeElementBase* OverlappedElement, AActor* OtherActor)
{
	if (!IsValid(OverlappedElement))
		return;

	ITSInteractable* InteractableInterface = Cast<ITSInteractable>(OtherActor);
	if (InteractableInterface == nullptr)
		return;

	if (OverlappedElement == SnakeHead)
	{
		InteractableInterface->Interact(this);
	}
}
