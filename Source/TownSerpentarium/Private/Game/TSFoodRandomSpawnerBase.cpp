// Copyright © 2023-2024 FaNtic, All rights reserved

#include "Game/TSFoodRandomSpawnerBase.h"
#include "Game/TSSnakeActorBase.h"
#include "Game/TSFoodBase.h"

ATSFoodRandomSpawnerBase::ATSFoodRandomSpawnerBase()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ATSFoodRandomSpawnerBase::BeginPlay()
{
	Super::BeginPlay();

	check(Snake);
	SpawnNewFood(Snake->AvaliableCoordinates);
}

ATSFoodBase* ATSFoodRandomSpawnerBase::SpawnNewFood(TArray<FIntPoint> SpawnCoordinates)
{
	if (SpawnCoordinates.Num() == 0)
		return nullptr;

	check(GetWorld());

	int32 RandomIndex = FMath::RandRange(0, SpawnCoordinates.Num() - 1);

	FVector FoodSpawnLocation = FVector(SpawnCoordinates[RandomIndex].X, SpawnCoordinates[RandomIndex].Y, FoodOffsetByZ);
	FTransform FoodSpawnTransform = FTransform(FoodSpawnLocation);

	TSubclassOf<ATSFoodBase> FoodClass;
	if (IsRareFood())
		FoodClass = RareFoodClass;
	else
		FoodClass = SimpleFoodClass;
	
	ATSFoodBase* NewFood;
	NewFood = GetWorld()->SpawnActorDeferred<ATSFoodBase>(FoodClass, FoodSpawnTransform);
	NewFood->FoodSpawner = this;
	NewFood->FinishSpawning(FoodSpawnTransform);

	return NewFood;
}

bool ATSFoodRandomSpawnerBase::IsRareFood()
{
	int32 Roll = FMath::RandRange(0, 100);

	if (Roll <= RareFoodChance)
		return true;
	else
		return false;
}