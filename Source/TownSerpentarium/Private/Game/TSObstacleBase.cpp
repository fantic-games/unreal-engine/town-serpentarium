// Copyright © 2023-2024 FaNtic, All rights reserved

#include "Game/TSObstacleBase.h"
#include "Game/TSSnakeActorBase.h"

ATSObstacleBase::ATSObstacleBase()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ATSObstacleBase::Interact(AActor* Interactor)
{
	ATSSnakeActorBase* SnakeActor = Cast<ATSSnakeActorBase>(Interactor);
	if (!IsValid(SnakeActor))
		return;

	SnakeActor->KillSnake();
}
