// Copyright © 2023-2024 FaNtic, All rights reserved

#include "Game/TSGridBase.h"

ATSGridBase::ATSGridBase()
{
	PrimaryActorTick.bCanEverTick = false;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Grid"));
	check(StaticMeshComponent);
	StaticMeshComponent->SetupAttachment(RootComponent);
}

void ATSGridBase::BeginPlay()
{
	Super::BeginPlay();

	UpdateScaleBySettings();
}

void ATSGridBase::UpdateScaleBySettings()
{
	int32 WorldWidth = Dimensions.Y * CellSize;
	int32 WorldHeight = Dimensions.X * CellSize;
	UStaticMesh* StaticMesh = StaticMeshComponent->GetStaticMesh();

	check(StaticMesh);
	const FBox BoundingBox = StaticMesh->GetBoundingBox();
	const FVector Size = BoundingBox.GetSize();

	check(Size.X);
	check(Size.Y);

	StaticMeshComponent->SetRelativeScale3D(FVector(WorldHeight / Size.X, WorldWidth / Size.Y, 1.0f));
}
