// Copyright © 2023-2024 FaNtic, All rights reserved

#include "Game/TSSnakeElementBase.h"
#include "Game/TSSnakeActorBase.h"

ATSSnakeElementBase::ATSSnakeElementBase()
{
	PrimaryActorTick.bCanEverTick = false;

	ElementStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Snake Element"));
	check(ElementStaticMeshComponent);
	ElementStaticMeshComponent->SetupAttachment(RootComponent);
	ElementStaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ElementStaticMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

void ATSSnakeElementBase::BeginPlay()
{
	Super::BeginPlay();

	ElementStaticMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ATSSnakeElementBase::HandleBeginOverlap);
}

void ATSSnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!IsValid(SnakeOwner))
		return;

	SnakeOwner->SnakeElementOverlapped(this, OtherActor);
}

void ATSSnakeElementBase::Interact(AActor* Interactor)
{
	ATSSnakeActorBase* SnakeActor = Cast<ATSSnakeActorBase>(Interactor);
	if (!IsValid(SnakeActor))
		return;

	SnakeActor->KillSnake();
}

void ATSSnakeElementBase::DestroyElementAfterTime(double DelayTime)
{
	FTimerHandle DestroyElementHandle;

	GetWorldTimerManager().SetTimer(DestroyElementHandle, this, &ATSSnakeElementBase::DestroyWithTimeElapsed, DelayTime, false);
}

void ATSSnakeElementBase::DestroyWithTimeElapsed()
{
	this->Destroy();
}
