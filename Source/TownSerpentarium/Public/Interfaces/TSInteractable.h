// Copyright © 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TSInteractable.generated.h"


UINTERFACE(MinimalAPI)
class UTSInteractable : public UInterface
{
	GENERATED_BODY()
};


class TOWNSERPENTARIUM_API ITSInteractable
{
	GENERATED_BODY()


public:

	virtual void Interact(AActor* Interactor);
};
