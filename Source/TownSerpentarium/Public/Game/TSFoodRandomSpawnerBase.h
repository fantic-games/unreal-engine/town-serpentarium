// Copyright © 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TSFoodRandomSpawnerBase.generated.h"

class ATSFoodBase;
class ATSSnakeActorBase;

UCLASS()
class TOWNSERPENTARIUM_API ATSFoodRandomSpawnerBase : public AActor
{
	GENERATED_BODY()
	
public:	

	ATSFoodRandomSpawnerBase();

public:

	UPROPERTY()
	ATSSnakeActorBase* Snake;
	
	UPROPERTY(EditDefaultsOnly, Category = "Food|Chances", meta = (ClampMin = 0, ClampMax = 100))
	int32 RareFoodChance = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Food")
	TSubclassOf<ATSFoodBase> SimpleFoodClass;

	UPROPERTY(EditDefaultsOnly, Category = "Food")
	TSubclassOf<ATSFoodBase> RareFoodClass;

	UPROPERTY(EditDefaultsOnly, Category = "Food|Offset")
	float FoodOffsetByZ = 65.0f;

protected:

	virtual void BeginPlay() override;

public:

	ATSFoodBase* SpawnNewFood(TArray<FIntPoint> SpawnCoordinates);

private:

	bool IsRareFood();

private:

	UPROPERTY()
	ATSFoodBase* SimpleFood;

	UPROPERTY()
	ATSFoodBase* RareFood;
};
