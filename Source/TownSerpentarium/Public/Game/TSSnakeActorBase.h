// Copyright © 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Containers/List.h"
#include "TSSnakeActorBase.generated.h"

class ATSSnakeElementBase;
class ATSPawnBase;

UENUM()
enum class EMovementDirection
{
	Up,
	Down,
	Left,
	Right
};

UCLASS()
class TOWNSERPENTARIUM_API ATSSnakeActorBase : public AActor
{
	GENERATED_BODY()
	
public:	

	ATSSnakeActorBase();

public:

	UPROPERTY()
	ATSPawnBase* Pawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<ATSSnakeElementBase> SnakeHeadClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<ATSSnakeElementBase> SnakeBodyClass;

	TDoubleLinkedList<ATSSnakeElementBase*> SnakeElements;

	UPROPERTY()
	ATSSnakeElementBase* SnakeHead;

	UPROPERTY()
	float ElementOffset = 100.0f;
	
	UPROPERTY()
	FVector ElementScale = FVector::OneVector;

	UPROPERTY()
	EMovementDirection LastMovementDirection = EMovementDirection::Right;

	UPROPERTY(EditDefaultsOnly)
	float UpdateMoveTime = 1.0f;

	bool SnakeMoved{ false };

	UPROPERTY()
	TArray<FIntPoint> AvaliableCoordinates;

	UPROPERTY(BlueprintReadOnly)
	bool GameStarted = false;

	UPROPERTY(EditDefaultsOnly)
	float StartGameDelay = 3.0f;

	UPROPERTY(BlueprintReadWrite)
	float TimeToStartLeft = StartGameDelay;

protected:

	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;

public:

	// Snake
	void InitSnake(uint32 SnakeLength);
	void AddSnakeElement(uint32 ElementsNum = 1);
	void KillSnake();

public:

	// Movement
	void Move();

public:

	// Element interaction
	UFUNCTION()
	void SnakeElementOverlapped(ATSSnakeElementBase* OverlappedElement, AActor* OtherActor);
	void UpdateAvaliableLocations();

private:

	void DestroyElement();
	void StartTickAfterDelay();
};
