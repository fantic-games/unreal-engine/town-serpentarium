// Copyright © 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TSGridBase.generated.h"

UCLASS()
class TOWNSERPENTARIUM_API ATSGridBase : public AActor
{
	GENERATED_BODY()
	
public:	

	ATSGridBase();

public:

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", meta = (ClampMin = "5", ClampMax = "100"))
	FIntPoint Dimensions {10, 10};

	UPROPERTY(BlueprintReadWrite, Category = "Settings")
	int32 CellSize  = 100;

protected:

	virtual void BeginPlay() override;

public:

	UFUNCTION(BlueprintCallable)
	void UpdateScaleBySettings();
};
