// Copyright © 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/TSInteractable.h"
#include "TSObstacleBase.generated.h"

class ATSSnakeActorBase;

UCLASS()
class TOWNSERPENTARIUM_API ATSObstacleBase : public AActor, public ITSInteractable
{
	GENERATED_BODY()
	
public:	

	ATSObstacleBase();

public:

	// Interactable Interface
	virtual void Interact(AActor* Interactor) override;
};
