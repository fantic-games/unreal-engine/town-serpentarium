// Copyright © 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/TSInteractable.h"
#include "TSFoodBase.generated.h"

class ATSSnakeActorBase;
class ATSFoodRandomSpawnerBase;

UCLASS()
class TOWNSERPENTARIUM_API ATSFoodBase : public AActor, public ITSInteractable
{
	GENERATED_BODY()
	
public:	

	ATSFoodBase();

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* FoodStaticMeshComponent;

	UPROPERTY(EditDefaultsOnly)
	int32 ElementsToAddAmount = 1;

	UPROPERTY()
	ATSFoodRandomSpawnerBase* FoodSpawner;

// Interface
public:

	virtual void Interact(AActor* Interactor) override;

};
