// Copyright © 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/TSInteractable.h"
#include "TSSnakeElementBase.generated.h"

class ATSSnakeActorBase;

UCLASS()
class TOWNSERPENTARIUM_API ATSSnakeElementBase : public AActor, public ITSInteractable
{
	GENERATED_BODY()
	
public:	

	ATSSnakeElementBase();

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* ElementStaticMeshComponent;

	UPROPERTY()
	ATSSnakeActorBase* SnakeOwner;

protected:

	virtual void BeginPlay() override;

public:

	// Interactable Interface
	virtual void Interact(AActor* Interactor) override;

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:

	// Element Destruction
	void DestroyElementAfterTime(double DelayTime);

private:

	void DestroyWithTimeElapsed();
};
