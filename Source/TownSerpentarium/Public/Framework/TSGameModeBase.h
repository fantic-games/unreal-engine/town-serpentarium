// Copyright © 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TSGameModeBase.generated.h"

class ATSGridBase;

UCLASS()
class TOWNSERPENTARIUM_API ATSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	virtual void BeginPlay() override;
};
