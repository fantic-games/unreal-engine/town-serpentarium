// Copyright © 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TSPawnBase.generated.h"

class UCameraComponent;
class ATSSnakeActorBase;
class ATSGridBase;
class ATSFoodRandomSpawnerBase;

UCLASS()
class TOWNSERPENTARIUM_API ATSPawnBase : public APawn
{
	GENERATED_BODY()

public:

	ATSPawnBase();

public:

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* CameraComponent;

	UPROPERTY(EditDefaultsOnly, category = "Settings|Camera", meta = (ClampMin = "0", ClampMax = "1"))
	float CameraOffsetByZ = 0.0f;

	UPROPERTY(EditDefaultsOnly, category = "Settings|Grid")
	FTransform GridTransform;

	UPROPERTY(EditDefaultsOnly, category = "Settings|Grid")
	TSubclassOf<ATSGridBase> GridClass;

	UPROPERTY()
	ATSGridBase* Grid;

	UPROPERTY(EditDefaultsOnly, category = "Settings|Snake")
	FVector SnakeSpawnOffset;

	UPROPERTY(EditDefaultsOnly, category = "Settings|Snake", meta = (ClampMin = "0.5", ClampMax = "1"))
	FVector SnakeElementScaleMultiplier = FVector::OneVector;

	UPROPERTY(EditDefaultsOnly, category = "Settings|Snake")
	TSubclassOf<ATSSnakeActorBase> SnakeClass;

	UPROPERTY(EditDefaultsOnly, category = "Settings|Random Food Spawner")
	TSubclassOf<ATSFoodRandomSpawnerBase> FoodSpawnerClass;

protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

private:

	// Camera
	void UpdatePawnLocationByGrid();
	void OnViewportResized(FViewport* Viewport, uint32 Value);

	// Inputs block
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void OnMoveVertical(float InputAxisValue);
	void OnMoveHorizontal(float InputAxisValue);

	//Grid
	void SpawnGrid();

	//Snake
	void SpawnSnake();

	// Food Spawner
	void SpawnFoodSpawner();

private:

	FDelegateHandle ViewportResizedHandle;

	UPROPERTY()
	ATSSnakeActorBase* Snake;

	UPROPERTY()
	ATSFoodRandomSpawnerBase* FoodSpawner;
};