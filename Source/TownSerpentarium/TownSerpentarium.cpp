// Copyright © 2023-2024 FaNtic, All rights reserved

#include "TownSerpentarium.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TownSerpentarium, "TownSerpentarium" );
